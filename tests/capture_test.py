#!/usr/bin/env python

import os
import time
import ctypes
ptr = ctypes.pointer
void_ptr = ctypes.c_void_p

gp = ctypes.CDLL('libgphoto2.so')
GP_CAPTURE_IMAGE = 0
ctxt = gp.gp_context_new()

cam = void_ptr()
print gp.gp_camera_new(ptr(cam))
print gp.gp_camera_init(cam, ctxt)

class CameraFilePath(ctypes.Structure):
    _fields_ = [
        ('name', (ctypes.c_char * 128)),
        ('folder', (ctypes.c_char * 1024))]                             

cp = CameraFilePath()
cp.name = 'foo.jpg'
cp.folder = '/'
print gp.gp_camera_capture(cam, 0, ptr(cp), ctxt)

cf = void_ptr()
filepath = os.path.join(os.getenv('HOME'), 'foo.jpg')
fd = os.open(filepath, os.O_WRONLY | os.O_CREAT)

print gp.gp_file_new_from_fd(ptr(cf), fd)
print gp.gp_camera_file_get(cam, cp.folder, cp.name, 1, ptr(cf), ctxt)
print gp.gp_camera_file_delete(cam, cp.folder, cp.name, ctxt)
print gp.gp_file_unref(cf)
gp.gp_camera_unref(cam)
gp.gp_context_unref(ctxt)


import os
from setuptools import setup

# ADIOS
# Astrophotography DSLR Imaging Open-Source

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "adios",
    version = "0.0.1",
    author = "Johan Nestaas",
    author_email = "johannestaas@gmail.com",
    description = "Astrophotography DSLR Imaging Open-Source",
    license = "GPLv3+",
    keywords = "astrophotography DSLR canon eos photography galaxy DSO stars",
    url = "https://bitbucket.org/johannestaas/adios",
    packages=['adios'],
    long_description=read('README'),
    classifiers=[
        #'Development Status :: 1 - Planning',
        #'Development Status :: 2 - Pre-Alpha',
        'Development Status :: 3 - Alpha',
        #'Development Status :: 4 - Beta',
        #'Development Status :: 5 - Production/Stable',
        #'Development Status :: 6 - Mature',
        #'Development Status :: 7 - Inactive',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Intended Audience :: Science/Research',
        'Intended Audience :: Other Audience',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Scientific/Engineering :: Astronomy',
        'Topic :: Scientific/Engineering :: Visualization',
        'Topic :: Scientific/Engineering :: Image Recognition',
        'Topic :: Multimedia :: Graphics :: Capture :: Digital Camera',
        'Environment :: Console',
        'Environment :: X11 Applications :: Qt',
        'Environment :: MacOS X',
        'Environment :: Win32 (MS Windows)',
        'Operating System :: POSIX',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: Microsoft :: Windows',
        'Programming Language :: Python',
        'Topic :: Utilities',
    ],
    install_requires=[
    ],
    entry_points = {
        'console_scripts': [
            'adios = adios.bin:',
        ],
    },
    package_data = {
        #'self_assessment': ['self_assessment/services.txt'],
    },
    #include_package_data = True,
)
